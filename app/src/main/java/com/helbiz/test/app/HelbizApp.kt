package com.helbiz.test.app
import android.app.Application
import com.helbiz.test.BuildConfig
import timber.log.Timber


class HelbizApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

    }
}