package com.helbiz.test.app

import android.content.Context
import com.helbiz.test.R
import com.helbiz.test.network.ApiConstants
import com.helbiz.test.network.response.POI
import com.helbiz.test.network.response.Restaurant
import com.helbiz.test.network.services.ApiService
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import timber.log.Timber


object AppRepoProvider {


    fun getAllRestaurant(service: ApiService, lat: Double, lng: Double, context: Context): Single<POI> {
        val parameters: MutableMap<String, String> = hashMapOf()
        parameters[ApiConstants.TAG_LABELS] = ApiConstants.TAG_LABELS_VALUE
        parameters[ApiConstants.FIELDS] = ApiConstants.FIELDS_VALUE
        parameters[ApiConstants.DISTANCE] = 5000.toString();
        parameters[ApiConstants.ANNOTATE] =
            String.format(context.resources.getString(R.string.distance_with_coordinates), lat, lng)
        parameters[ApiConstants.COUNT] = 50.toString();
        parameters[ApiConstants.ORDER_BY] = ApiConstants.DISTANCE;
        Timber.e("Map for api is $parameters")
        return service.getAllRestaurant(parameters)
    }

    fun getRestaurantDetailsById(service: ApiService, id:String): Single<POI> {
        return service.getRestaurantDetailsById(id)
    }

}