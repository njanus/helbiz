package com.helbiz.test.app

class DataConstants {
    companion object {
        const val SPLASH_DELAY_TIME: Long = 2000 //4 seconds
        const val LOCATION_PERMISSION: Int = 100
        const val UPDATE_INTERVAL = (120 * 1000).toLong()  /*  */
        const val FASTEST_INTERVAL: Long = 2000 /* 2 sec */
        const val RESTAURANT: String = "restaurant" /* 2 sec */
    }

}