package com.helbiz.test.network.services


import com.helbiz.test.network.ApiConstants
import com.helbiz.test.network.response.POI
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ApiService {


    @GET(ApiConstants.POINT_OF_INTEREST)
    fun getAllRestaurant(@QueryMap parameters: Map<String, String>): Single<POI>

    @GET(ApiConstants.POINT_OF_INTEREST)
    fun getRestaurantDetailsById(@Query(ApiConstants.ID) id: String): Single<POI>

}