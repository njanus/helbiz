package com.helbiz.test.network.response

import java.io.Serializable

data class Restaurant(
    var name: String,
    var distance: Int,
    var coordinates: Coordinates,
    var score: Double,
    var eatingout_score: Double,
    var id: String,
    var snippet: String,
    var images: List<Images>
) : Serializable
