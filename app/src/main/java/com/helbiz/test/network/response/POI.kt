package com.helbiz.test.network.response

data class POI(
    var results: List<Restaurant>,
    var estimated_total: Int,
    var more: Boolean
)
