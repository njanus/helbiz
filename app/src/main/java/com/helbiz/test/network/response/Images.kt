package com.helbiz.test.network.response

data class Images(

    val attribution: Attribution,
    val license: String,
    val sizes: Sizes,
    val owner: String,
    val source_url: String,
    val caption: String,
    val source_id: String,
    val owner_url: String
)