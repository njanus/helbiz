package  com.helbiz.test.network.response


data class Attribution (

	val license_link : String,
	val attribution_link : String,
	val attribution_text : String,
	val license_text : String,
	val format : String
)