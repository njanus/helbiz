package  com.helbiz.test.network.response


data class Sizes (

	val medium : Medium,
	val original : Original,
	val thumbnail : Thumbnail
)