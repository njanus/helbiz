package com.helbiz.test.network.response

import java.io.Serializable


data class Coordinates(
    var latitude: Double,
    var longitude: Double
) : Serializable
