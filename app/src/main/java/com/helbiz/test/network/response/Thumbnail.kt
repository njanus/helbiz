package  com.helbiz.test.network.response


data class Thumbnail (

	val url : String,
	val width : Int,
	val format : String,
	val bytes : Int,
	val height : Int
)