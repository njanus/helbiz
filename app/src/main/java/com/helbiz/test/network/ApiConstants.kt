package com.helbiz.test.network

class ApiConstants {
    companion object {
        const val HTTP_TIMEOUTS: Long = 25000 // 25s
        const val BASE_URL = "https://www.triposo.com/api/20181213/"

        // HEADERS
        const val HEADER_KEY_CONTENT_TYPE = "Content-Type"
        const val HEADER_VALUE_CONTENT_TYPE_JSON = "application/json"
        const val HEADER_VALUE_ACCEPT = "Accept"
        const val X_ACCOUNT_KEY = "X-Triposo-Account"
        const val X_ACCOUNT_VALUE = "YDIYVMO2"
        const val X_TOKEN_KEY = "X-Triposo-Token"
        const val X_TOKEN_VALUE = "7982cexehuvb40itknddvk3et5rlu2lx"


        // ENDPOINTS
        const val POINT_OF_INTEREST = "poi.json"
        const val TAG_LABELS = "tag_labels"
        const val TAG_LABELS_VALUE = "eatingout"
        const val FIELDS = "fields"
        const val FIELDS_VALUE = "id,name,score,coordinates"
        const val DISTANCE = "distance"
        const val ANNOTATE = "annotate"
        const val COUNT = "count"
        const val ORDER_BY = "order_by"
        const val ID = "id"

        //HTTP CODE
        const val HTTP_OK = 200
        const val SUCCESS_AUTH = 201
        const val UNAUTHORIZED_ERROR = 401
    }

}