package  com.helbiz.test.network.response

data class Medium(

    val url: String,
    val width: Int,
    val format: String,
    val bytes: Int,
    val height: Int
)