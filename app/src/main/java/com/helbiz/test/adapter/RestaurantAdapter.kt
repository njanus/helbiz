package com.helbiz.test.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.helbiz.test.R
import com.helbiz.test.network.response.Restaurant


class RestaurantAdapter(
    private var context: Context,
    private var restaurants: ArrayList<Restaurant>,
    private var listener: OnItemClickedListener
) :
    RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolderItem>() {

    private lateinit var restaurant: Restaurant
//    private lateinit var listener: OnItemClickedListener


    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): RestaurantViewHolderItem {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_restaurant, parent, false)
        return RestaurantViewHolderItem(view)
    }

    override fun onBindViewHolder(@NonNull holder: RestaurantViewHolderItem, position: Int) {

        val restaurant: Restaurant = restaurants.get(position)
        holder.itemClicked.setOnClickListener {
            if (listener != null) {
                listener.onItemCicked(restaurant)
            }
        }
        holder.name.text = restaurant.name
        holder.distance.text = String.format(context.resources.getString(R.string.distance), restaurant.distance)

    }


    override fun getItemCount(): Int {
        return restaurants.size
    }

    inner class RestaurantViewHolderItem internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var name: TextView = itemView.findViewById(R.id.textViewName)
        var distance: TextView = itemView.findViewById(R.id.textViewDistance)
        var itemClicked: RelativeLayout = itemView.findViewById(R.id.rlRestaurant)

    }

    interface OnItemClickedListener {
        fun onItemCicked(restaurant: Restaurant)
    }
}
