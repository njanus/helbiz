package com.helbiz.test.views

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.helbiz.test.R
import com.helbiz.test.adapter.RestaurantAdapter
import com.helbiz.test.app.AppRepoProvider
import com.helbiz.test.app.DataConstants
import com.helbiz.test.network.ApiClient
import com.helbiz.test.network.response.POI
import com.helbiz.test.network.response.Restaurant
import com.helbiz.test.network.services.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.util.*


class MainActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, LocationListener, OnMapReadyCallback,
    RestaurantAdapter.OnItemClickedListener {

    private lateinit var poiDisposable: Disposable
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: RestaurantAdapter
    private lateinit var mGoogleApiClient: GoogleApiClient
    private lateinit var locationManager: LocationManager
    private lateinit var mLocation: Location
    private val restaurant: Restaurant? = null

    private var restaurants: MutableList<Restaurant> = arrayListOf()
    private var mLocationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null

    private var mGoogleMap: GoogleMap? = null
    private var mvMap: MapView? = null


    private var latitude: Double = 0.0
    private var longitude: Double = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mvMap = flMap
        mvMap?.onCreate(savedInstanceState)
        mvMap?.onResume()
        mvMap?.getMapAsync(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    DataConstants.LOCATION_PERMISSION
                )
            }

        }

        initView()
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        mLocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        checkLocation()
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mvMap?.onSaveInstanceState(outState)
    }

    private fun checkLocation(): Boolean {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private fun isLocationEnabled(): Boolean {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun showAlert() {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Enable Location")
            .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app")
            .setPositiveButton("Location Settings", DialogInterface.OnClickListener { _, _ ->
                val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(myIntent)
            })
            .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() })
        dialog.show()
    }

    private fun startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(DataConstants.UPDATE_INTERVAL)
        //  .setFastestInterval(DataConstants.FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient,
            mLocationRequest, this
        )
    }

    override fun onStart() {
        super.onStart();
        mGoogleApiClient.connect()
        mvMap?.onStart()
    }

    private fun initView() {
        recyclerView = rvRestaurants
        val mLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager
        adapter = RestaurantAdapter(this, restaurants as ArrayList<Restaurant>, this)
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun getData(latitude: Double, longitude: Double) {
        progressBar.visibility = View.VISIBLE
        val service = AppRepoProvider.getAllRestaurant(
            ApiClient.getRetrofit().create(ApiService::class.java),
            latitude,
            longitude,
            this
        )
        poiDisposable = service.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { response -> showRestaurant(response) },
                { error -> handleError(error) }
            )
    }

    private fun handleError(error: Throwable?) {
        Timber.e("OnError  " + error.toString())
        progressBar.visibility = View.GONE
        textViewNoRestaurant.visibility = View.VISIBLE
    }

    private fun showRestaurant(response: POI?) {
        Timber.e("showRestaurant " + response.toString())
        progressBar.visibility = View.GONE
        textViewNoRestaurant.visibility = View.GONE
        restaurants.clear()
        if (response != null) {
            restaurants.addAll(response.results)
        }
        adapter.notifyDataSetChanged()

        restaurants.forEach {
            mGoogleMap?.addMarker(
                MarkerOptions().position(LatLng(it.coordinates.latitude, it.coordinates.longitude)).title(it.name)
                    .icon(BitmapDescriptorFactory.defaultMarker(Random().nextInt(360).toFloat()))
            )?.showInfoWindow()
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        poiDisposable.dispose()
        if (mGoogleApiClient.isConnected) {
            mGoogleApiClient.disconnect();
        }
        mvMap?.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        mvMap?.onResume()
    }

    public override fun onPause() {
        super.onPause()
        mvMap?.onPause()
    }

    override fun onConnected(p0: Bundle?) {
        Timber.e("Connection onConnected");
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            return;
        }

        startLocationUpdates();

        val fusedLocationProviderClient: FusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(this);
        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener(this) { location ->
                if (location != null) {
                    // Logic to handle location object
                    mLocation = location;
                    latitude = location.latitude
                    longitude = location.longitude
                    Timber.e("addOnSuccessListener, latitude " + latitude + " and long " + longitude)
                    mGoogleMap?.addMarker(
                        MarkerOptions().position(LatLng(latitude, longitude)).title("My locaton")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                    )
                    mGoogleMap?.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                latitude,
                                longitude
                            ), 13f
                        )
                    )
                }
                getData(latitude, longitude)
            }
    }


    override fun onConnectionSuspended(p0: Int) {
        Timber.e("Connection Suspended");
        mGoogleApiClient.connect();
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Timber.e("Connection failed. Error: " + connectionResult.errorCode);
    }

    override fun onLocationChanged(location: Location?) {
        if (location != null) {
            latitude = location.latitude
            longitude = location.longitude
            Timber.e("onLocationChanged, latitude " + latitude + "and long " + longitude);
        }
        Timber.e("onLocationChanged: $location");
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            DataConstants.LOCATION_PERMISSION ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onConnected(null)
                    textViewNoRestaurant.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE

                } else {
                    Toast.makeText(this, getString(R.string.loc_permission), Toast.LENGTH_LONG).show()
                    textViewNoRestaurant.visibility = View.VISIBLE
                    progressBar.visibility = View.GONE
                    recyclerView.visibility = View.GONE
                }
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mGoogleMap = googleMap
        // THis is click listners fo  map marker, currently commented
        // mGoogleMap?.setOnMarkerClickListener { openDetailsActivity(restaurant) }

    }

    private fun openDetailsActivity(restaurant: Restaurant) {
        val intent = Intent(this, RestaurantDetailsActivity::class.java)
        intent.putExtra(DataConstants.RESTAURANT, restaurant)
        startActivity(intent)
    }

    override fun onItemCicked(restaurant: Restaurant) {
        openDetailsActivity(restaurant)
    }


}

