package com.helbiz.test.views

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.helbiz.test.R
import com.helbiz.test.app.AppRepoProvider
import com.helbiz.test.app.DataConstants
import com.helbiz.test.network.ApiClient
import com.helbiz.test.network.response.POI
import com.helbiz.test.network.response.Restaurant
import com.helbiz.test.network.services.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_details_restaurant.*

class RestaurantDetailsActivity : AppCompatActivity() {
    private lateinit var restaurant: Restaurant
    private lateinit var poiDisposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_restaurant)
        if (intent != null) {
            restaurant = intent.getSerializableExtra(DataConstants.RESTAURANT) as Restaurant
        }
        getDataFromServer()
    }

    private fun getDataFromServer() {
        progressBarDetails.visibility = View.VISIBLE
        val service = AppRepoProvider.getRestaurantDetailsById(
            ApiClient.getRetrofit().create(ApiService::class.java),
            restaurant.id
        )
        poiDisposable = service.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { response -> showRestaurantDetails(response) },
                { error -> handleError(error) }
            )
    }

    private fun handleError(error: Throwable?) {
        progressBarDetails.visibility = View.GONE
        Toast.makeText(this, getString(R.string.default_error), Toast.LENGTH_LONG).show()
    }
/// This should be in some Utils class
    private fun roundDigits(value: Double, places: Int): Double {
        var value = value
        if (places < 0) throw IllegalArgumentException()

        val factor = Math.pow(10.0, places.toDouble()).toLong()
        value *= factor
        val tmp = Math.round(value)
        return tmp.toDouble() / factor
    }

    private fun showRestaurantDetails(response: POI?) {
        var restaurant: Restaurant? = null
        if (response != null) {
            restaurant = response.results[0]
        }
        progressBarDetails.visibility = View.GONE
        if (restaurant != null) {
            tvRestaurantName.text = String.format(resources.getString(R.string.name), restaurant.name)
            tvRestaurantDesctrition.text = String.format(resources.getString(R.string.description), restaurant.snippet)
            var rating: Double = roundDigits(restaurant.score, 2)
            tvRestaurantRating.text = String.format(resources.getString(R.string.rating), rating)
            var url: String = ""
            if (restaurant.images.isNotEmpty() && restaurant.images[0].sizes != null && restaurant.images[0].sizes.medium != null) {
                url = restaurant.images[0].sizes.medium.url
            }
            Glide.with(this).load(url)
                .apply(RequestOptions.placeholderOf(R.mipmap.ic_launcher))
                .into(ivRestaurantIcon)
        } else {
            Toast.makeText(this, getString(R.string.loc_permission), Toast.LENGTH_LONG).show()
        }

    }

    public override fun onDestroy() {
        super.onDestroy()
    }


}



